# dSights Bulk API Repo #

This README would be summary doucment for accessing various bulk APIs for deployed models by dSights. The examples would be written on 
python **requests** module,could be customized for other access methods like Curl, Postman and Node Express etc.

### What is this repository for? ###

* Examples of Accessing various ML Models deployed by [dSights](https://www.dsightsonline.com)
* Version : v1.0
* Date Updated : 02-08-2020

### How do I get set up? ###

* APIs for various models would be seperated by folders with name of the models. In each folder we will would have __requirements.txt__ file which would be helpful for setting the **Bulk API** operation.
* Refer Model Folders and __requirements.txt__ files.
* Basic Module :: Python 3.7, requests, Numpy and Pandas.
* Each folder will Jupyter Notebook,which would explain the details...

### Access Restrictions ###

* Beta Phase, these will have *public access* or *unauthernticated*.
* Token or JWT based access would be place in soon,please contact as per below if you have issues with respect to access.
* All models are **deployed** in *Severless  Docker Containers* may face some **cold Start** issues at the beginging. 

### Who do I talk to? ###
* __info@dsighstsonline.com__
